if Gitlab.config.sentry.enabled
  Raven.configure do |config|
    config.dsn = Gitlab.config.sentry.dsn
  end
end
